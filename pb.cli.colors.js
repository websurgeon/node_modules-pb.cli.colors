/*jslint node:false */

(function (exports) {
    "use strict";

    exports.reset   = '\u001b[0m';
    exports.black   = '\u001b[30m';
    exports.red     = '\u001b[31m';
    exports.green   = '\u001b[32m';
    exports.yellow  = '\u001b[33m';
    exports.blue    = '\u001b[34m';
    exports.magenta = '\u001b[35m';
    exports.cyan    = '\u001b[36m';
    exports.white   = '\u001b[37m';
    exports.grey    = '\u001b[90m';

}(exports));
